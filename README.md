@Authored - KA20072509 / kajal.sharma1@wipro.com

@Project - ILP1 - Continous Integration and deployement.

@Tools Used - Java, maven, eclipse, git/gitlab, Jenkins, Jfrog and Tomcat.

@Brief -

The Bookstore project source code is built using java as a core language with Junit for Unit Testing framework in the ide - Eclipse. Build management tool such as Maven is used for the purpose
of Project dependencies check, build. The code is pused to Gitlab source management tool.

For building the CI/CD job - I have used Jenkins here further integrating it with Jfrog artifactory and deploying it to Tomcat Server.

Note - This all activities are done for ILP1. For coming ILP2 - Readme file will be updated with the latest changes.